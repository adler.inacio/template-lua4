<!--

FORMA 01 - COM SLIDER
-------------------------------------------------------------------------------------------------
-->

<div class="destaques">

    <?php
        
        foreach ($dados['carros'] as $banner) {
            $foto = $banner['fotos'][0];
            ?>
                <div class="card-slider">
                    <div class="card-header" style="background-image: url(<?= $foto['arquivo'] ?>);"> 
                        <a href="#" class="btn-comparar"> Comparar </a>
                    </div>
                    <div class="card-title">
                        <h4><a href="#"><?= $banner['modelo']." ".$banner['versao'] ?></a></h4>
                        <h5><?= $banner['combustivel'].", ".$banner['portas']."P ".$banner['cambio'].", ANO: ".$banner['ano_fabricacao']."/".$banner['ano_modelo'] ?></h5>
                        <h4 class="price-destaque"><?= "R$ ".number_format($banner['preco_site'], 2, ',', '.') ?></h4>
                    </div>
                    <div class="card-footer">
                        <a href="#" class="btn-oferta"> Confira Oferta </a>
                    </div>
                </div>
            <?
        }
        
    ?>

</div>

<!-- Scripts do Slider -->
<script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>

<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

<script type="text/javascript">
    $(document).ready(function(){
        $('.destaques').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
            {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
            }
            },
            {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
            },
            {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
        });
    });
</script>

<!--

FORMA 02 - NORMAL
-------------------------------------------------------------------------------------------------
-->

<?php
    /*
    foreach ($dados['carros'] as $banner) {
        $foto = $banner['fotos'][0];
        ?>
            <div class="col-1-4">
                <div class="card-header" style="background-image: url(<?= $foto['arquivo'] ?>);"> 
                    <a href="#" class="btn-comparar"> Comparar </a>
                </div>
                <div class="card-title">
                    <h4><a href="#"><?= $banner['modelo']." ".$banner['versao'] ?></a></h4>
                    <h5><?= $banner['combustivel'].", ".$banner['portas']."P ".$banner['cambio'].", ANO: ".$banner['ano_fabricacao']."/".$banner['ano_modelo'] ?></h5>
                    <h4 class="price-destaque"><?= "R$ ".number_format($banner['preco_site'], 2, ',', '.') ?></h4>
                </div>
                <div class="card-footer">
                    <a href="#" class="btn-oferta"> Confira Oferta </a>
                </div>
            </div>
        <?
    }
    */
?>